COOKIE="Cookie: PHPSESSID=$1"
POSTRES="action=authenticate&login=$2&password=$3&policy_accept=true&from_ajax=true&wispr_mode=false"
echo $POSTRES

curl -d "$POSTRES" -X POST https://etudiants.campusea.fr/portal_api.php \
    -H "Host: etudiants.campusea.fr" \
    -H "Accept: application/json, text/javascript, */*; q=0.01" \
    -H "Accept-Language: en-US,en;q=0.5" \
    -H "Accept-Encoding: gzip, deflate, br" \
    -H "Content-Type: application/x-www-form-urlencoded" \
    -H "X-Requested-With: XMLHttpRequest" \
    -H "Content-Length: 106" \
    -H "Origin: https://etudiants.campusea.fr" \
    -H "Connection: keep-alive" \
    -H "Referer: https://etudiants.campusea.fr/104/portal/" \
    -H "Sec-Fetch-Dest: empty" \
    -H "Sec-Fetch-Mode: cors" \
    -H "Sec-Fetch-Site: same-origin" \
    -H "User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0" \
    -H "$COOKIE"
