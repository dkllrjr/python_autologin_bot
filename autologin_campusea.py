from selenium import webdriver
from selenium.webdriver.firefox.options import Options

from pathlib import Path
import os
from sys import argv
from time import sleep
from random import random
from random import choice
from subprocess import call

import yaml

def login(username, password):
    options = Options()
    options.headless = True
    driver = webdriver.Firefox(options=options, executable_path=Path('geckodriver'))
    driver.get('https://etudiants.campusea.fr/104/portal')
    cookie = driver.get_cookie('PHPSESSID')['value']

    stream = os.popen(argv[2] + '/login_campusea.sh ' + cookie + ' ' + username + ' ' + password)
    output = stream.read()
    print(output)

def ping(hostname, params):

    command = ['ping'] + params + [hostname]

    return call(command) == 0

with open(Path(argv[1]), 'r') as file:
    data = yaml.load(file)

username = data['username']
password = data['password']

while True:
    delay = 300
    random_delay = random()*delay
    domains = ['4.2.2.2','4.2.2.1','8.8.8.8','google.com','gmail.com']
    if ping(choice(domains),['-c','1']):
        print('sleeping between internet pings for about: ' + str(int(random_delay)) + ' seconds')
        sleep(random_delay)
    else:
        login(username, password)

